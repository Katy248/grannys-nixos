# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  nix.settings.experimental-features = [
    "nix-command" "flakes"
  ];
  # Bootloader.
  boot = { 
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
  };

  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Moscow";

  # Select internationalisation properties.
  i18n =
  let
    russian = "ru_RU.UTF-8";
    english = "en_US.UTF-8";
  in
  {
    defaultLocale = english;
    extraLocaleSettings = {
      LC_ADDRESS = russian;
      LC_IDENTIFICATION = russian;
      LC_MEASUREMENT = russian;
      LC_MONETARY = russian;
      LC_NAME = russian;
      LC_NUMERIC = russian;
      LC_PAPER = russian;
      LC_TELEPHONE = russian;
      LC_TIME = russian;
    };
  };

  # Enable the X11 windowing system.
  services = {
    xserver = {
      enable = true;
      # Enable the GNOME Desktop Environment.
      displayManager.gdm.enable = true;
      desktopManager.gnome.enable = true;
      # Configure keymap in X11
      layout = "us,ru";
      xkbVariant = "";
    };

    pipewire = {
      enable = true;
      
      alsa = {
        enable = true;
        support32Bit = true;
      };

      pulse.enable = true;
      #jack.enable = true;
    };

    # Enable CUPS for printing
    printing = {
      enable = true;
      drivers = with pkgs; [
        # Drivers for Brother printers
        brlaser brgenml1lpr brgenml1cupswrapper
      ];
    };
    
    # Sync service
    syncthing = {
      enable = true;
      user = "nina";
      #openDefaultPorts = true;
      dataDir = /home/nina;
    };
  };

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.nina = {
    name = "nina";
    isNormalUser = true;
    description = "nina";
    extraGroups = [ "networkmanager" "wheel" ];
    packages = with pkgs; [];
  };

  # Allow unfree packages
  nixpkgs = {
    config = {
      allowUnfree = true;
      permittedInsecurePackages = [
        "electron-25.9.0"
      ];
    };
  };


  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    # Base editing apps
    neovim helix
    nil # language server
    # Cli utils
    git fastfetch
    # Desktop settings
    gnome.gnome-tweaks
    # Base apps
    google-chrome obsidian
    # home manager
    home-manager
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?

}
